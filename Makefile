DESTDIR =
prefix = /usr/local
sbindir = $(prefix)/sbin
pkglibdir = $(prefix)/lib/array-info
mandir = $(prefix)/share/man

subdirs = lib plugins include

INCLUDES = -I./include -I./linuxheaders

CFLAGS = -g2 -Wall $(INCLUDES) -DARRAY_PLUGIN_PATH=\"$(ARRAY_PLUGIN_PATH)\"
LDFLAGS = -L./lib -larray-info -ldl
OBJS = array_plugin.o array_utils.o main.o

ARRAY_PLUGIN_PATH=$(pkglibdir)/plugins

OUTPUT = array-info
MAN = array-info.1

REL_NAME = array-info
REL_VER = 0.16
DISTDIR=$(REL_NAME)-$(REL_VER)
TARGZ=$(REL_NAME)_$(REL_VER).tar.gz
TARBZ2=$(REL_NAME)_$(REL_VER).tar.bz2

DOCBOOK2XMAN=docbook2x-man


DISTLIST = $(shell ls | grep -v $(TARGZ) | grep -v $(TARBZ2)| grep -v $(DISTDIR))


all : build_lib build_plugins $(OUTPUT) $(MAN).gz

build_lib : 
	make -C lib

build_plugins :	
	make -C plugins

$(OUTPUT) : $(OBJS)
	$(CC) -o $(OUTPUT) $(OBJS) $(LDFLAGS)

$(MAN).gz :  $(MAN).docbook
	$(DOCBOOK2XMAN) $(MAN).docbook
	-gzip --best $(MAN)

install : all $(MAN).gz
	mkdir -p $(DESTDIR)$(sbindir)/.
	cp $(OUTPUT) $(DESTDIR)$(sbindir)/.
	for dir in $(subdirs) ; do \
		$(MAKE) -C $$dir $@ DESTDIR=$(DESTDIR) prefix=$(prefix); \
	done
	mkdir -p $(DESTDIR)$(mandir)/man1
	cp $(MAN).gz $(DESTDIR)$(mandir)/man1/

release : indent clean
	cd .. && cp -a $(REL_NAME) $(REL_NAME)-$(REL_VER) && tar czf $(REL_NAME)-$(REL_VER).tar.gz $(REL_NAME)-$(REL_VER)

indent : 
	find . -name "*.[ch]" -exec indent -kr -i8 -ts8 -sob -l80 -ss -bs -psl {} \; && find . -name "*~" -exec rm {} \;

clean distclean ::
	for dir in $(subdirs) ; do \
		$(MAKE) -C $$dir $@ DESTDIR=$(DESTDIR) prefix=$(prefix); \
	done
clean ::
	rm -f *~ $(OBJS)

distclean :: clean
	rm -f *~ $(OBJS) $(OUTPUT) $(MAN).gz $(TARGZ) $(TARBZ2)

changelog :
	rm -f ChangeLog
	cvs2cl --utc -U Contributors -I ChangeLog

dist : distclean
	rm -rf $(DISTDIR)
	rm -f $(TARGZ) $(TARBZ2)
	mkdir $(DISTDIR)
	cp -r $(DISTLIST) $(DISTDIR)
	-find $(DISTDIR) -type d -name CVS -exec rm -rf {} \;
	tar czf $(TARGZ) $(DISTDIR)
	tar cjf $(TARBZ2) $(DISTDIR)
	rm -rf $(DISTDIR)
