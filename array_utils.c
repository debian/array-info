/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 *
 * $Log: array_utils.c,v $
 * Revision 1.6  2002/07/29 16:49:10  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.5  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "array_info.h"

void *
xmalloc(int size)
{
	void *addr;

	if (!(addr = malloc(size))) {
		printf("Cannot allocate memory...\n");
	}
	return addr;
}

array_data_t *
open_ctrl(char *device_path)
{
	int fd;
	struct stat stat_buf;
	array_data_t *array_data;

	if ((fd = open(device_path, O_RDONLY)) < 0)
		perror("open");
	fstat(fd, &stat_buf);
	if (!(S_ISBLK(stat_buf.st_mode) || (S_ISCHR(stat_buf.st_mode)))) {
		printf("%s is not a valid block or char device...\n",
		       device_path);
		return NULL;
	}

	if (!(array_data = xmalloc(sizeof (*array_data))))
		return NULL;
	array_data->fd = fd;
	array_data->device_major.type =
	    S_ISBLK(stat_buf.st_mode) ? BLOCK_DEVICE : CHAR_DEVICE;
	array_data->device_major.major = major(stat_buf.st_rdev);
	array_data->device_major.minor = minor(stat_buf.st_rdev);

	return array_data;
}

void
close_ctrl(array_data_t * ctrl_data)
{
	close(ctrl_data->fd);

	free(ctrl_data);
}
