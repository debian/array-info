/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 * Copyright (c) 2009  Google, Inc (ragnark@google.com)
 *
 * $Log: main.c,v $
 * Revision 1.10  2009/09/18 17:40:22  ragnark
 * Add information about spare disks for cciss plugin
 * Make array-info exit with exit code 1 if it detects problems with any of the logical volumes.
 *
 * Revision 1.9  2007/02/06 16:48:28  pere
 * Print the default plugin path in the usage info block.
 *
 * Revision 1.8  2002/07/30 14:12:46  trez42
 * Functions add and show infos
 *
 * Revision 1.7  2002/07/29 16:55:16  trez42
 * Trivial fix
 *
 * Revision 1.6  2002/07/29 16:49:10  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.5  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <getopt.h>

#include "array_info.h"
#include "array_plugin.h"

void
print_version()
{
	printf("%s v%d.%d.%d by Benoit Gaussen (ben@trez42.net)\n\n",
	       ARRAY_INFO_RELEASE_NAME,
	       ARRAY_INFO_VERSION_MAJOR,
	       ARRAY_INFO_VERSION_MINOR, ARRAY_INFO_VERSION_PATCH);
	printf("http://sourceforge.net/projects/array-info\n\n");
}

void
print_help()
{
	printf("%s v%d.%d.%d by Benoit Gaussen (ben@trez42.net)\n\n",
	       ARRAY_INFO_RELEASE_NAME,
	       ARRAY_INFO_VERSION_MAJOR,
	       ARRAY_INFO_VERSION_MINOR, ARRAY_INFO_VERSION_PATCH);
	printf
	    ("Usage:\n"
	     "  %s -d array_device_path [-a|-l|-A|-c|-s|-L|-h]\n\n"
	     "Options:\n"
	     "  --device=PATH      -d       path to array device\n"
	     "  --all-drives       -a       show informations about all drives\n"
	     "  --logical-drive    -l       show informations about selected logical drive\n"
	     "  --show-ctrl        -c       show informations about controller\n"
	     "  --show-logical     -L       show informations about logical drives\n"
	     "  --show-status      -s       show status of logical drives\n"
	     "  --show-all         -A       show all informations\n"
	     "  --plugin-path=PATH          path to plugins directory (%s)\n"
	     "  --plugin-list               show plugin list\n"
	     "  --version          -V       show version\n"
	     "  --help             -h       display this help message\n\n",
	     ARRAY_INFO_RELEASE_NAME, ARRAY_PLUGIN_PATH);
}

void
array_info_exit(array_plugin_list_t * plugin_list, array_data_t * array_data,
		int ret)
{
	if (plugin_list)
		array_close_plugins(plugin_list);
	if (array_data)
		close_ctrl(array_data);
	exit(ret);
}

int
main(int argc, char **argv)
{
	u_int8_t query_flags = 0;
	u_int8_t dump_flags = 0;
	char *device_path;
	char *plugin_path = NULL;
	array_plugin_list_t *plugin_list = NULL;
	array_data_t *array_data = NULL;
	int opt_ok = 0, opt_sh_plugin = 0;

	while (1) {
		int c, option_index = 0;
		static struct option long_options[] = {
			{"device", required_argument, NULL, 'd'},
			{"all-drives", no_argument, NULL, 'a'},
			{"log-drive", no_argument, NULL, 'l'},
			{"show-all", no_argument, NULL, 'A'},
			{"show-satus", no_argument, NULL, 's'},
			{"show-ctrl", no_argument, NULL, 'c'},
			{"show-log", no_argument, NULL, 'L'},
			{"plugin-path", required_argument, NULL, '0'},
			{"plugin-list", no_argument, NULL, '1'},
			{"version", no_argument, NULL, 'V'},
			{"help", no_argument, NULL, 'h'},
			{0, 0, 0, 0}
		};

		if ((c =
		     getopt_long(argc, argv, "d:alAscL0:1Vh", long_options,
				 &option_index)) == -1)
			break;

		switch (c) {
		case 'a':
			query_flags = QUERY_ALL_LDRV;
			break;
		case 'l':
			query_flags = QUERY_SELECTED_LDRV;
			break;
		case 'd':
			opt_ok = 1;
			device_path = optarg;
			break;
		case 'A':
			dump_flags = ARRAY_INFO_LEVEL_ALL;
			break;
		case 's':
			dump_flags |= ARRAY_INFO_LEVEL_LDRV_STATUS;
			break;
		case 'c':
			dump_flags |= ARRAY_INFO_LEVEL_CTRL;
			break;
		case 'L':
			dump_flags |= ARRAY_INFO_LEVEL_LDRV;
			break;
		case '0':
			plugin_path = optarg;
			break;
		case '1':
			opt_sh_plugin = 1;
			break;
		case 'V':
			print_version();
			return 0;
		default:
			print_help();
			return 0;
		}
	}

	if ((opt_ok == 0) && (opt_sh_plugin == 0)) {
		print_help();
		return 0;
	}

	if (!plugin_path)
		plugin_path = ARRAY_PLUGIN_PATH;
	plugin_list = array_load_plugins(plugin_path);

	if (opt_sh_plugin == 1) {
		array_show_plugins(plugin_list);
		array_info_exit(plugin_list, array_data, 0);
	}

	if (!(array_data = open_ctrl(device_path))) {
		array_info_exit(plugin_list, array_data, -1);
	}

	array_data->query_flags = query_flags ? query_flags : QUERY_ALL_LDRV;
	array_data->dump_flags = dump_flags ? dump_flags : ARRAY_INFO_LEVEL_ALL;
	int errors = array_dispatch(plugin_list, array_data);

	array_info_exit(plugin_list, array_data, errors ? 1 : 0);
	return 0;
}
