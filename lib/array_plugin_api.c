/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 *
 * $Log: array_plugin_api.c,v $
 * Revision 1.3  2007/02/01 14:42:35  pere
 * Indent.
 *
 * Revision 1.2  2002/07/30 14:12:46  trez42
 * Functions add and show infos
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdarg.h>

#include "array_plugin.h"

void
array_show_infos(array_infos_t * info, char level)
{
	unsigned char min_level = 0xff;
	array_infos_t *tail;

	while (info) {
		if (level & info->level)
			if (info->level < min_level)
				min_level = info->level;
		tail = info;
		info = info->next;
	}
	while (tail) {
		if (level & tail->level)
			printf("%s", tail->string);
		tail = tail->prev;
	}
}

void
array_add_infos(array_infos_t ** info, char level, char *string, ...)
{
	va_list ap;
	array_infos_t *new;
	char *final_str;
	int nb;

	new = malloc(sizeof (array_infos_t));
	final_str = malloc(80);
	new->level = level;
	va_start(ap, string);
	va_end(ap);
	if ((nb = vsnprintf(final_str, 80, string, ap)) > 80) {
		free(final_str);
		final_str = malloc(nb);
		vsnprintf(final_str, nb, string, ap);
	}
	new->string = final_str;
	new->next = *info;
	new->prev = NULL;

	if (*info)
		(*info)->prev = new;

	*info = new;
}
