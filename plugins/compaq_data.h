/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 * Copyright (c) 2009  Google, Inc (ragnark@google.com)
 *
 * $Log: compaq_data.h,v $
 * Revision 1.2  2009/09/18 17:40:22  ragnark
 * Add information about spare disks for cciss plugin
 * Make array-info exit with exit code 1 if it detects problems with any of the logical volumes.
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.2  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _COMPAQ_DATA_H_
#define _COMPAQ_DATA_H_

typedef struct {
	u_int8_t log_drive;
	u_int32_t nr_blks;
	u_int16_t blk_size;
	u_int8_t fault_tol;
	u_int8_t status;
	u_int32_t blks_to_recover;
	u_int8_t spare_stat;
} compaq_log_drive_t;

typedef struct {
	u_int32_t board_id;
	u_int8_t nr_ldrvs;
	u_int8_t firm_rev[4];
	u_int8_t rom_rev[4];

	compaq_log_drive_t *ldrvs;
} compaq_ctrl_t;

#endif				/* COMPAQ_DATA_H_ */
