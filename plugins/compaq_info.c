/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 *
 * $Log: compaq_info.c,v $
 * Revision 1.12  2009/09/18 17:40:22  ragnark
 * Add information about spare disks for cciss plugin
 * Make array-info exit with exit code 1 if it detects problems with any of the logical volumes.
 *
 * Revision 1.10  2008/05/22 21:34:03  pere
 * Add ID for RAID controller HP Smart Array P400.
 *
 * Revision 1.9  2007/02/21 16:28:40  pere
 * Add ID for raid controllers HP Smart Array E200i.
 *
 * Revision 1.8  2007/02/07 17:17:43  pere
 * Changed the 'status is ok' message for the md plugin to match the one
 * in the cciss and ida plugin, and make the string common across all
 * plugins.
 *
 * Revision 1.7  2007/02/05 15:46:38  pere
 * Add ID for raid controller Compaq Smart Array 64xx.
 *
 * Revision 1.6  2007/02/05 13:35:02  pere
 * Remove empty line at the end of the status block.
 *
 * Revision 1.5  2007/02/03 10:04:05  pere
 * Convert the cciss and ida plugin to the new shared library framework.  Some code cleanup.
 *
 * Revision 1.4  2007/01/25 13:31:08  pere
 * Include controller id when reporting an unknown controller.
 *
 * Revision 1.3  2007/01/25 13:29:06  pere
 * Add ID for raid controller HP Smart Array E200.
 *
 * Revision 1.2  2007/01/25 11:14:07  pere
 * Fix typo in status message.
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.4  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "array_plugin.h"

#include "array_info.h"
#include "compaq_info.h"

/* RAID level */
static const char *compaq_raid_level_str[] = {
	"RAID 0 (Stripping)",
	"RAID 4",
	"RAID 1 (Mirroring)",
	"RAID 5",
	"RAID 5+1",
	"Unknown RAID code (5)",
	"Unknown RAID code (6)"
};

/* Status of logical drive */
static const char *compaq_ldrv_status_str[] = {
	LOGICAL_VOLUME_OK,
	"Logical drive is failed",
	"Logical drive is not configured",
	"Logical drive is using interim recovery mode",
	"Logical drive is ready for recovery operation",
	"Logical drive is currently recovering",
	"Wrong physical drive was replaced",
	"A physical drive is not properly connected",
	"Hardware is overheating",
	"Hardware has overheated",
	"Logical drive is currently expanding",
	"Logical drive is not yet available",
	"Logical drive is queued for expansion",
};

/* Status of spare disks. */
static const char *compaq_ldrv_spare_status_str[] = {
	"Configured",
	"Rebuilding",
	"Rebuilt",
	"Failed",
	"Activated",
	"Available",
};

static struct array_board compaq_boards[] = {
	{0x0040110E, "Compaq IDA"},
	{0x0140110E, "Compaq IDA-2"},
	{0x1040110E, "Compaq IAES"},
	{0x2040110E, "Compaq SMART"},
	{0x3040110E, "Compaq SMART-2/E"},
	{0x3211103C, "HP Smart Array E200i"},
	{0x3212103C, "HP Smart Array E200"},
	{0x3234103C, "HP Smart Array P400"},
	{0x40300E11, "Compaq SMART-2/P"},
	{0x40310E11, "Compaq SMART-2SL"},
	{0x40320E11, "Compaq Smart Array 3200"},
	{0x40330E11, "Compaq Smart Array 3100ES"},
	{0x40340E11, "Compaq Smart Array 221"},
	{0x40400E11, "Compaq Integrated Array"},
	{0x40480E11, "Compaq Raid LC2"},
	{0x40500E11, "Compaq Smart Array 4200"},
	{0x40510E11, "Compaq Smart Array 4250ES"},
	{0x40580E11, "Compaq Smart Array 431"},
	{0x40700E11, "Compaq Smart Array 5300"},
	{0x40800E11, "Compaq Smart Array 5i"},
	{0x40820E11, "Compaq Smart Array 532"},
	{0x40910e11, "Compaq Smart Array 64xx"},
	{0, 0}
};

static char *
array_id2str(struct array_board *arrays, u_int32_t id)
{
	static char unknownstr[512];
	int i = 0;

	while (arrays[i].id) {
		if (arrays[i].id == id)
			return arrays[i].name;
		i++;
	}
	snprintf(unknownstr, sizeof (unknownstr),
		 "Unknown Controller id 0x%x", id);
	return unknownstr;
}

static void
compaq_ldrv_infos(compaq_ctrl_t * ctrl, array_infos_t * compaq_infos,
		  array_plugin_callbacks_t * callbacks, u_int8_t ldrv,
		  int * errors)
{
	callbacks->array_add_infos(&compaq_infos,
				   ARRAY_INFO_LEVEL_LDRV,
				   "\tLogical drive %2d :\n"
				   "\t  Fault tolerance : %s\n",
				   ldrv,
				   compaq_raid_level_str[ctrl->ldrvs[ldrv].
							 fault_tol]);
	callbacks->array_add_infos(&compaq_infos, ARRAY_INFO_LEVEL_LDRV,
				   "\t  Size            : %2.2f GiB (%u blocks of %d bytes)\n",
				   (double) ctrl->ldrvs[ldrv].nr_blks *
				   ctrl->ldrvs[ldrv].blk_size / 1024 / 1024 /
				   1024, ctrl->ldrvs[ldrv].nr_blks,
				   ctrl->ldrvs[ldrv].blk_size);

	callbacks->array_add_infos(&compaq_infos,
				   ARRAY_INFO_LEVEL_LDRV_STATUS,
				   "\t  Status          : %s",
				   compaq_ldrv_status_str[ctrl->ldrvs[ldrv].
							  status]);
	if (ctrl->ldrvs[ldrv].status != LDRV_STATUS_OK) {
		(*errors)++;
	}

	switch (ctrl->ldrvs[ldrv].status) {
	case LDRV_STATUS_INTERIMRECOVERY:
	case LDRV_STATUS_RECOVERING:
	case LDRV_STATUS_EXPANDINGLDRV:{
			double percentdone =
			    100 *
			    (double) (ctrl->ldrvs[ldrv].nr_blks -
				      ctrl->ldrvs[ldrv].blks_to_recover) /
			    (double) ctrl->ldrvs[ldrv].nr_blks;
			callbacks->array_add_infos(&compaq_infos,
						   ARRAY_INFO_LEVEL_LDRV_STATUS,
						   "\t(%3.2f%% done)\n",
						   percentdone);
			break;
		}
	default:
		callbacks->array_add_infos(&compaq_infos,
					   ARRAY_INFO_LEVEL_LDRV_STATUS, "\n");
		break;
	}

	/* Status of spare disks: */
	callbacks->array_add_infos(&compaq_infos, ARRAY_INFO_LEVEL_LDRV_STATUS,
		                   "\t  Spare           : ");
	if (ctrl->ldrvs[ldrv].spare_stat & 1 << LDRV_SPARE_STATUS_CONFIGURED) {
		int i;
		for (i=0; i < LDRV_SPARE_STATUS_LAST; i++) {
			if (ctrl->ldrvs[ldrv].spare_stat & 1 << i) {
				callbacks->array_add_infos(&compaq_infos, ARRAY_INFO_LEVEL_LDRV_STATUS,
						           "%s ", compaq_ldrv_spare_status_str[i]);
			}
		}
		callbacks->array_add_infos(&compaq_infos, ARRAY_INFO_LEVEL_LDRV_STATUS, "\n");
	} else {
		callbacks->array_add_infos(&compaq_infos, ARRAY_INFO_LEVEL_LDRV_STATUS,
					   "Not configured\n");
	}
	if (ctrl->ldrvs[ldrv].spare_stat & 1 << LDRV_SPARE_STATUS_CONFIGURED &&
	    !(ctrl->ldrvs[ldrv].spare_stat & 1 << LDRV_SPARE_STATUS_AVAILABLE)) {
		/* Consider it an error if a spare disk is configured but is not
		 * available for use. This may be the result of the spare having
		 * failed or already being activated. */
		(*errors)++;
	}
}

array_infos_t *
compaq_ctrl_infos(compaq_ctrl_t * ctrl, array_plugin_callbacks_t * callbacks,
		  u_int8_t query_flags, u_int8_t log_drive,
		  int * errors)
{
	array_infos_t *compaq_infos = NULL;
	callbacks->array_add_infos(&compaq_infos,
				   ARRAY_INFO_LEVEL_CTRL,
				   "%s\n",
				   array_id2str(compaq_boards, ctrl->board_id));
	callbacks->array_add_infos(&compaq_infos, ARRAY_INFO_LEVEL_CTRL,
				   "\tFirmware revision : %c%c%c%c\n"
				   "\tRom revision      : %c%c%c%c\n",
				   ctrl->firm_rev[0],
				   ctrl->firm_rev[1],
				   ctrl->firm_rev[2],
				   ctrl->firm_rev[3],
				   ctrl->rom_rev[0],
				   ctrl->rom_rev[1],
				   ctrl->rom_rev[2], ctrl->rom_rev[3]);
	callbacks->array_add_infos(&compaq_infos, ARRAY_INFO_LEVEL_CTRL,
				   "\t%-2d logical drive%s configured.\n\n",
				   ctrl->nr_ldrvs,
				   ctrl->nr_ldrvs > 1 ? "s" : "");

	if (query_flags == QUERY_ALL_LDRV) {
		u_int8_t i;
		for (i = 0; i < ctrl->nr_ldrvs; i++)
			compaq_ldrv_infos(ctrl, compaq_infos, callbacks, i, errors);
	} else
		compaq_ldrv_infos(ctrl, compaq_infos, callbacks, log_drive, errors);
	return compaq_infos;
}
