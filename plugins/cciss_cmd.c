/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 *
 * $Log: cciss_cmd.c,v $
 * Revision 1.2  2007/02/03 10:04:05  pere
 * Convert the cciss and ida plugin to the new shared library framework.  Some code cleanup.
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.3  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "array_info.h"
#include "cciss_info.h"

int
cciss_bmic_cmd(cciss_ctrl_data_t * ctrl_data, int opcode, int dir, void *buf,
	       int buf_size)
{
	IOCTL_Command_struct cciss_io;
	struct cciss_bmic_cdb *bmic_cdb;
	int ret;

	memset(&cciss_io, 0, sizeof (cciss_io));

	cciss_io.Request.Type.Type = TYPE_CMD;
	cciss_io.Request.Type.Attribute = ATTR_SIMPLE;
	cciss_io.Request.Type.Direction =
	    dir == CCISS_CMD_READ ? XFER_READ : XFER_WRITE;
	cciss_io.Request.Timeout = 0;

	cciss_io.Request.CDBLen = sizeof (*bmic_cdb);
	bmic_cdb = (void *) cciss_io.Request.CDB;
	bmic_cdb->opcode =
	    dir == CCISS_CMD_READ ? OPCODE_CDB_READ : OPCODE_CDB_WRITE;
	bmic_cdb->bmic_opcode = opcode;
	bmic_cdb->log_drive = ctrl_data->log_drive;
	bmic_cdb->size = buf_size;

	cciss_io.buf = buf;
	cciss_io.buf_size = buf_size;

	if ((ret = ioctl(ctrl_data->fd, CCISS_PASSTHRU, &cciss_io)) < 0) {
		perror("ioctl");
		return ret;
	}

	return cciss_io.error_info.CommandStatus;
}
