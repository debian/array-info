/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 *
 * $Log: md_data.h,v $
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.5  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _MD_DATA_H_
#define _MD_DATA_H_

typedef struct {
	unsigned long array_size;
	int device_size;
	int fault_tol;
	int status;
	int errors;
	int creation_time;
	int nr_disks;
	int raid_disks;
	int active_disks;
	int working_disks;
	int failed_disks;
	int spare_disks;
	struct mdu_disk_info_s *disks;
} md_log_drive_t;

typedef struct {
	int vers_major;
	int vers_minor;
	int vers_patchlevel;

	md_log_drive_t ldrv;
} md_ctrl_t;

typedef struct {
	int fd;

	md_ctrl_t md_ctrl;
} md_ctrl_data_t;

#endif				/* MD_DATA_H_ */
