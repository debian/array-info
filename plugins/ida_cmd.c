/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 *
 * $Log: ida_cmd.c,v $
 * Revision 1.2  2007/02/03 10:04:05  pere
 * Convert the cciss and ida plugin to the new shared library framework.  Some code cleanup.
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.3  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "array_info.h"
#include "ida_info.h"

#include "xmalloc.ic"

ida_ioctl_t *
ida_bmic_cmd(ida_ctrl_data_t * ctrl_data, u_int8_t opcode)
{
	ida_ioctl_t *ida_io;
	int ret;

	ida_io = xmalloc(sizeof (*ida_io));
	memset(ida_io, 0, sizeof (*ida_io));

	ida_io->cmd = opcode;

	if ((ret = ioctl(ctrl_data->fd, IDAPASSTHRU, ida_io)) < 0) {
		perror("ioctl");
		return NULL;
	}

	return ida_io;
}
