/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 *
 * $Log: md_cmd.c,v $
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.2  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "array_info.h"

#include "md_info.h"

int
md_cmd(int fd, int opcode, void *data)
{
	int ret;

	if ((ret = ioctl(fd, opcode, data)) < 0) {
		perror("ioctl");
		return ret;
	}

	return ret;
}
