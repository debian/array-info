/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 * Copyright (c) 2009  Google, Inc (ragnark@google.com)
 *
 * $Log: cciss_info.h,v $
 * Revision 1.3  2009/09/18 17:26:42  ragnark
 * Add link to compaq firmware documentation
 *
 * Revision 1.2  2007/02/03 10:04:05  pere
 * Convert the cciss and ida plugin to the new shared library framework.  Some code cleanup.
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.4  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _CCISS_INFO_H_
#define _CCISS_INFO_H_

#include <sys/types.h>
#include <linux/cciss_ioctl.h>
#include "compaq_data.h"

#define CCISS_INFO_VERSION         ((0<<16) + (0<<8) + 0)

#define CCISS_CMD_READ  0
#define CCISS_CMD_WRITE 1

struct cciss_bmic_cdb {
	u_int8_t opcode;
#define OPCODE_CDB_READ         0x26
#define OPCODE_CDB_WRITE        0x27
	u_int8_t log_drive;
	u_int8_t phys_drive;
	u_int8_t reserverd1[3];
	u_int8_t bmic_opcode;
#define OPCODE_BMIC_IDLOG       0x10
#define OPCODE_BMIC_IDCTRL      0x11
#define OPCODE_BMIC_SENSELOG    0x12
	u_int16_t size;
	u_int8_t reserved2;
} __attribute__ ((packed));

/* The datastructures are documented in the firmware documentation available from 
 * http://sourceforge.net/projects/cciss/files/Firmware%20documentation/fwspecwww.doc/download
*/

struct id_ctrl {
	u_int8_t nr_drvs;
	u_int32_t cfg_sig;
	u_int8_t firm_rev[4];
	u_int8_t rom_rev[4];
	u_int8_t hw_rev;
	u_int32_t bb_rev;
	u_int32_t drv_present_map;
	u_int32_t ext_drv_map;
	u_int32_t board_id;
	u_int8_t cfg_error;
	u_int32_t non_disk_bits;
	u_int8_t bad_ram_addr;
	u_int8_t cpu_rev;
	u_int8_t pdpi_rev;
	u_int8_t epic_rev;
	u_int8_t wcxc_rev;
	u_int8_t marketing_rev;
	u_int8_t ctlr_flags;
	u_int8_t host_flags;
	u_int8_t expand_dis;
	u_int8_t scsi_chips;
	u_int32_t max_req_blocks;
	u_int32_t ctlr_clock;
	u_int8_t drvs_per_bus;
	u_int16_t big_drv_present_map[8];
	u_int16_t big_ext_drv_map[8];
	u_int16_t big_non_disk_map[8];
	u_int16_t task_flags;
	u_int8_t icl_bus;
	u_int8_t red_modes;
	u_int8_t cur_red_mode;
	u_int8_t red_ctlr_stat;
	u_int8_t red_fail_reason;
	u_int8_t reserved[403];
} __attribute__ ((packed));

struct drv_param {
	u_int16_t cyl;
	u_int8_t heads;
	u_int8_t xsig;
	u_int8_t psectors;
	u_int16_t wpre;
	u_int8_t maxecc;
	u_int8_t drv_ctrl;
	u_int16_t pcyls;
	u_int8_t pheads;
	u_int16_t landz;
	u_int8_t sect_per_track;
	u_int8_t cksum;
} __attribute__ ((packed));

struct id_log_drv {
	u_int16_t blk_size;
	u_int32_t nr_blks;
	struct drv_param drv;
	u_int8_t fault_tol;
	u_int8_t reserved;
	u_int8_t bios_disable;
} __attribute__ ((packed));

struct sense_log_drv {
	u_int8_t status;
	u_int32_t fail_map;
	u_int16_t read_err[32];
	u_int16_t write_err[32];
	u_int8_t drv_err_data[256];
	u_int8_t drq_timeout[32];
	u_int32_t blks_to_recover;
	u_int8_t drv_recovering;
	u_int16_t remap_cnt[32];
	u_int32_t replace_drv_map;
	u_int32_t act_spare_map;
	u_int8_t spare_stat;
	u_int8_t spare_repl_map[32];
	u_int32_t repl_ok_map;
	u_int8_t media_exch;
	u_int8_t cache_fail;
	u_int8_t expn_fail;
	u_int8_t unit_flags;
	u_int16_t big_fail_map[8];
	u_int16_t big_remap_map[128];
	u_int16_t big_repl_map[8];
	u_int16_t big_act_spare_map[8];
	u_int8_t big_spar_repl_map[128];
	u_int16_t big_repl_ok_map[8];
	u_int8_t big_drv_rebuild;
	u_int8_t reserved[36];
} __attribute__ ((packed));

typedef struct {
	int vers_major;
	int vers_minor;
	int vers_patchlevel;

	struct id_ctrl ctrl_info;
	compaq_log_drive_t ldrv;
} cciss_ctrl_t;

typedef struct {
	int fd;

	compaq_ctrl_t ctrl;
	u_int8_t log_drive;
	u_int8_t query_flags;
} cciss_ctrl_data_t;

/* Prototypes */

int cciss_bmic_cmd(cciss_ctrl_data_t * ctrl_data, int opcode,
		   int dir, void *buf, int buf_size);

#endif				/* _CCISS_INFO_H_ */
