/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 * Copyright (c) 2009  Google, Inc (ragnark@google.com)
 *
 * $Log: compaq_info.h,v $
 * Revision 1.3  2009/09/18 17:40:22  ragnark
 * Add information about spare disks for cciss plugin
 * Make array-info exit with exit code 1 if it detects problems with any of the logical volumes.
 *
 * Revision 1.2  2007/02/03 10:04:05  pere
 * Convert the cciss and ida plugin to the new shared library framework.  Some code cleanup.
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.3  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _COMPAQ_INFO_H_
#define _COMPAQ_INFO_H_

#include "ida_info.h"
#include "cciss_info.h"

#define COMPAQ_INFO_VERSION             ((0<<16) + (0<<8) + 0)

#define LDRV_STATUS_OK                          0
#define LDRV_STATUS_FAILED                      1
#define LDRV_STATUS_NOTCONFIGURED               2
#define LDRV_STATUS_INTERIMRECOVERY             3
#define LDRV_STATUS_READYFORRECOVERY            4
#define LDRV_STATUS_RECOVERING                  5
#define LDRV_STATUS_WRONGPHYSDRVREPLACED        6
#define LDRV_STATUS_PHYSDRVBADCONN              7
#define LDRV_STATUS_EXPANDINGLDRV               10
#define LDRV_STATUS_LDRVNOTAVAILABLE            11
#define LDRV_STATUS_LDRVQUEUEDEXPANSION         12

#define LDRV_SPARE_STATUS_CONFIGURED		0
#define LDRV_SPARE_STATUS_REBUILDING		1
#define LDRV_SPARE_STATUS_REBUILT		2
#define LDRV_SPARE_STATUS_FAILED		3
#define LDRV_SPARE_STATUS_ACTIVATED		4
#define LDRV_SPARE_STATUS_AVAILABLE		5
#define LDRV_SPARE_STATUS_LAST			6


/* Prototypes */

array_infos_t *compaq_ctrl_infos(compaq_ctrl_t * ctrl,
				 array_plugin_callbacks_t * callbacks,
				 u_int8_t query_flags, u_int8_t log_drive,
				 int * errors);

#endif				/* COMPAQ_INFO_H_ */
