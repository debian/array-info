/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 *
 * $Log: md_info.h,v $
 * Revision 1.4  2007/02/01 14:43:26  pere
 * Rewrite plugin API to pass driver data between the functions.
 *
 * Revision 1.3  2007/01/31 13:32:29  pere
 * Remove the need to link libarray-info into plugins by providing the functions needed in a list of callbacks.
 *
 * Revision 1.2  2007/01/25 23:09:33  pere
 * Reduce the number of exported symbols to make sure the array_plugin interface is used.
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.9  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _MD_INFO_H_
#define _MD_INFO_H_

#include <linux/raid/md_u.h>
#include <linux/fs.h>

#define MD_INFO_VERSION         ((0<<16) + (12<<8) + 0)

/*
 * Superblock state bits from md_p.h
 */
#define MD_SB_CLEAN     0
#define MD_SB_ERRORS    1
/*
 * Device "operational" state bits from md_p.h
 */
#define MD_DISK_FAULTY     0	/* disk is faulty / operational */
#define MD_DISK_ACTIVE     1	/* disk is running or spare disk */
#define MD_DISK_SYNC    2	/* disk is in sync with the raid set */
#define MD_DISK_REMOVED    3	/* disk is in sync with the raid set */

#define MD_RAID_RAID0 0
#define MD_RAID_RAID1 1
#define MD_RAID_RAID5 2
#define MD_RAID_LINEAR 3
#define MD_RAID_TRANSLUCENT 4
#define MD_RAID_HSM 5
#define MD_RAID_MULTIPATH 6
#define MD_RAID_UNKNOWN 7

#endif				/* _MD_INFO_H_ */
