/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 *
 * $Log: ida_info.h,v $
 * Revision 1.2  2007/02/03 10:04:05  pere
 * Convert the cciss and ida plugin to the new shared library framework.  Some code cleanup.
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.4  2002/07/25 16:51:14  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _IDA_INFO_H_
#define _IDA_INFO_H_

#include <ida_ioctl.h>
#include <ida_cmd.h>
#include "compaq_data.h"

#define IDA_INFO_VERSION         ((0<<16) + (0<<8) + 0)

typedef struct {
	int fd;

	compaq_ctrl_t ctrl;
	u_int8_t log_drive;
	u_int8_t query_flags;
} ida_ctrl_data_t;

/* Prototypes */

ida_ioctl_t *ida_bmic_cmd(ida_ctrl_data_t * ctrl_data, u_int8_t opcode);

#endif				/* _IDA_INFO_H_ */
