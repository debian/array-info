/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 * Copyright (c) 2009  Google, Inc (ragnark@google.com)
 *
 * $Log: array_info.h,v $
 * Revision 1.5  2009/09/18 17:58:48  ragnark
 * Update to version 0.16
 *
 * Revision 1.4  2009/09/18 17:40:22  ragnark
 * Add information about spare disks for cciss plugin
 * Make array-info exit with exit code 1 if it detects problems with any of the logical volumes.
 *
 * Revision 1.3  2007/01/25 23:13:10  pere
 * Bump version number.
 *
 * Revision 1.2  2002/07/30 14:12:46  trez42
 * Functions add and show infos
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 * Revision 1.4  2002/07/25 16:47:42  trez42
 * CVS Fixes
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _ARRAY_INFO_H_
#define _ARRAY_INFO_H_

#include <linux/major.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>

#define ARRAY_INFO_RELEASE_NAME	"array-info"

#define ARRAY_INFO_VERSION_MAJOR 	0
#define ARRAY_INFO_VERSION_MINOR 	16
#define ARRAY_INFO_VERSION_PATCH 	0

#define ARRAY_INFO_VERSION	(((ARRAY_INFO_VERSION_MAJOR)<<16) + ((ARRAY_INFO_VERSION_MINOR)<<8) + (ARRAY_INFO_VERSION_PATCH))

#define VERSION_MAJOR(a)	(a>>16 & 0xff)
#define VERSION_MINOR(a)	(a>>8 & 0xff)
#define VERSION_PATCH(a)	(a & 0xff)

typedef struct {
#define BLOCK_DEVICE 1
#define CHAR_DEVICE 2
	char type;
	char major;
	char minor;
} device_major_t;

#define ARRAY_INFO_LEVEL_CTRL (1<<0)
#define ARRAY_INFO_LEVEL_LDRV (1<<1)
#define ARRAY_INFO_LEVEL_LDRV_STATUS (1<<2)
#define ARRAY_INFO_LEVEL_ALL  (0xff)

typedef struct {
	int fd;
	device_major_t device_major;

	u_int8_t query_flags;
#define QUERY_SELECTED_LDRV	(1<<0)
#define QUERY_ALL_LDRV			(1<<1)

	u_int8_t dump_flags;
} array_data_t;

typedef struct array_infos_s {
	char level;
	char *string;

	struct array_infos_s *prev;
	struct array_infos_s *next;
} array_infos_t;

struct array_board {
	u_int32_t id;
	char *name;
};

/* Prototypes */

array_data_t *open_ctrl(char *device_path);
void close_ctrl(array_data_t * array_data);

#endif				/* _ARRAY_INFO_H_ */
