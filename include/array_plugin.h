/*
 * array-info - Array Controllers Informations
 * Copyright (C) 2002  Benoit Gaussen (ben@trez42.net)
 * Copyright (c) 2009  Google, Inc (ragnark@google.com)
 *
 * $Log: array_plugin.h,v $
 * Revision 1.8  2009/09/18 17:40:22  ragnark
 * Add information about spare disks for cciss plugin
 * Make array-info exit with exit code 1 if it detects problems with any of the logical volumes.
 *
 * Revision 1.7  2007/02/07 17:17:43  pere
 * Changed the 'status is ok' message for the md plugin to match the one
 * in the cciss and ida plugin, and make the string common across all
 * plugins.
 *
 * Revision 1.6  2007/02/01 14:43:26  pere
 * Rewrite plugin API to pass driver data between the functions.
 *
 * Revision 1.5  2007/01/31 13:32:29  pere
 * Remove the need to link libarray-info into plugins by providing the functions needed in a list of callbacks.
 *
 * Revision 1.4  2007/01/25 23:02:41  pere
 * Correct the prototypes.
 *
 * Revision 1.3  2007/01/25 15:14:06  pere
 * Make sure the install target installs the plugins.
 *
 * Revision 1.2  2002/07/30 14:12:46  trez42
 * Functions add and show infos
 *
 * Revision 1.1  2002/07/29 16:50:19  trez42
 * Add plugin support
 * Change directory structure
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _ARRAY_PLUGIN_H_
#define _ARRAY_PLUGIN_H_

#include "array_info.h"

typedef struct array_plugin_callbacks_s {
	void (*array_add_infos) (array_infos_t ** info, char level,
				 char *string, ...);
} array_plugin_callbacks_t;

typedef void *driver_data_ptr_t;

typedef struct {
	device_major_t *device_majors;

	u_int32_t plugin_version;
	u_int32_t array_info_version;

	driver_data_ptr_t driver_data;

	char *(*plugin_description) (void);
	 driver_data_ptr_t(*ctrl_open) (array_data_t *);
	int (*ctrl_close) (driver_data_ptr_t);
	int (*ctrl_req) (driver_data_ptr_t);
	array_infos_t *(*ctrl_infos) (driver_data_ptr_t,
				      array_plugin_callbacks_t * callbacks,
				      int *errors);
	void (*ctrl_free) (driver_data_ptr_t);
} array_plugin_t;

typedef struct array_plugin_list_s {
	array_plugin_t *plugin;
	void *plugin_handle;
	char *plugin_path;
	struct array_plugin_list_s *next;
} array_plugin_list_t;

// Prototypes

array_plugin_list_t *array_load_plugins(char *so_path);
void array_close_plugins(array_plugin_list_t * plugin_list);
void array_show_plugins(array_plugin_list_t * plugin_list);
int array_dispatch(array_plugin_list_t * plugin_list,
		   array_data_t * array_data);

// Plugin API
void array_show_infos(array_infos_t * info, char level);
void array_add_infos(array_infos_t ** info, char level, char *string, ...);

/* Make sure all plugins report an OK volume the same way */
#define LOGICAL_VOLUME_OK "Logical drive is ok"

#endif				// _ARRAY_PLUGIN_H_
